<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

function admin_logged_in()
{
    $CI =& get_instance();
    $is_logged_in = $CI->session->userdata('username');
    if(!isset($is_logged_in) || $is_logged_in != true)
    {
        redirect('frontend/login');
    }       
}

function alumni_logged_in()
{
    $CI =& get_instance();
    $is_logged_in = $CI->session->userdata('nisn');
    if(!isset($is_logged_in) || $is_logged_in != true)
    {
        redirect('frontend/login');
    }
}

function cmb_dinamis($name, $table, $field, $pk, $selected = null, $extra = null) {
    $ci = & get_instance();
    $cmb = "<select name='$name' class='form-control' $extra>  ";
    $data = $ci->db->get($table)->result();
    foreach ($data as $row) {
        $cmb .="<option value='" . $row->$pk . "'";
        $cmb .= $selected == $row->$pk ? 'selected' : '';
        $cmb .=">" . $row->$field . "</option>";
    }
    $cmb .= "</select>";
    return $cmb;
}

if (!function_exists('log_all')) {
    function log_all()
    {
        if (ENVIRONMENT == 'production') {

        } else {
            log_message('error', "________________________________________________ START LOG ________________________________________________");
            $log1 = print_r(apache_request_headers(), true);
            $log2 = ($_SERVER['REQUEST_METHOD']);
            $log3 = print_r($_REQUEST, true);
            $log4 = print_r(apache_response_headers(), true);
            log_message('error', "________________________________________________  REQUEST   ________________________________________________");
            log_message('error', $log1);
            log_message('error', $log2 . " = " . $log3 . " ");
            log_message('error', "________________________________________________  REPONSE   ________________________________________________");
            log_message('error', $log4);
            log_message('error', '________________________________________________  END LOG  ________________________________________________');

        }
    }
}

if (!function_exists('log_app')) {
    /**
     * Menampilkan Log
     *
     *
     * @param   String $string Input Text
     */
    function log_app($string)
    {
        if (ENVIRONMENT == 'production') {

        } else {
            log_message('error', "IP : " . $_SERVER['REMOTE_ADDR'] . " Pesan : " . $string);
        }

    }
}

if (!function_exists('get_config_app')) {
    /**
     * Menampilkan Log
     *
     *
     * @param   String $string Input Text
     */
    function get_config_app($string)
    {
        $ci = & get_instance();
        $ci->load->database();
        $ci->db->select('content');
        $ci->db->from('config');
        $ci->db->where('id_config',$string);
        $query = $ci->db->get();
        return $query->row()->content;
    }
}