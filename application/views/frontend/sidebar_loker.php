<div class="col-md-3">
    <div class="sidebar">
        <div class="panel panel-info">
            <div class="panel-heading">
                <span class="text-center"><h3 class="panel-title" >Loker Saya</h3></span>
            </div>

            <div class="panel-body">
                <ul class="recent-posts clearfix">
                    <?php foreach ($agenda_terbaru as $a):?>
                        <li>
                            <div class="text">
                                <a href="<?php echo site_url();?>/agenda/lihatagenda/<?=$a['id_agenda']?>"><i class="fa fa-caret-right"></i> <?=$a['judul_agenda']?></a>
                            </div>
                        </li>
                        <br>
                    <?php endforeach ?>
                    <a class="btn btn-primary no-border" href="<?php echo site_url('loker/tambah');?>">Tambah Loker <i class="fa fa-angle-right"></i></a>
                </ul>
            </div>
        </div>
    </div>

    <div class="sidebar">
    <div class="panel panel-info">
        <div class="panel-heading">
            <span class="text-center"><h3 class="panel-title" >Lowongan Kerja Terbaru</h3></span>
        </div>
        
        <div class="panel-body">
            <ul class="recent-posts clearfix">
                <?php foreach ($loker_terbaru as $b):?>
                    <li>
                        <div class="text">
                            <a href="<?php echo site_url();?>/loker/lihatloker/<?=$b['id_loker']?>"><i class="fa fa-caret-right"></i> Lowongan Kerja <?=$b['judul_loker']?></a>
                        </div>
                    </li>
                    <br>
                <?php endforeach ?>
                <a class="btn btn-primary no-border" href="<?php echo site_url('loker');?>">Selengkapnya <i class="fa fa-angle-right"></i></a>
            </ul>
        </div>
    </div>
</div>

    <div class="sidebar">
    <div class="panel panel-info">
        <div class="panel-heading">
            <span class="text-center"><h3 class="panel-title" >Agenda Terbaru</h3></span>
        </div>
        
        <div class="panel-body">
            <ul class="recent-posts clearfix">
                <?php foreach ($agenda_terbaru as $a):?>
                    <li>
                        <div class="text">
                            <a href="<?php echo site_url();?>/agenda/lihatagenda/<?=$a['id_agenda']?>"><i class="fa fa-caret-right"></i> <?=$a['judul_agenda']?></a>
                        </div>
                    </li>
                    <br>
                <?php endforeach ?>
                <a class="btn btn-primary no-border" href="<?php echo site_url('agenda');?>">Selengkapnya <i class="fa fa-angle-right"></i></a>
            </ul>
        </div>
    </div>
</div>

</div>




