
<div class="tp-banner-container">
    <div class="tp-banner" >
        <ul>
            <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?php echo base_url();?>uploads/web/<?=$slider1 ?>" alt="slider-image">

                <div class="tp-caption sft desc-slide center color-white color-full" data-x="770" data-y="279" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">
                    <div class="title main-color-1 font-2">Alumni <?php echo get_config_app(2)?></div>
                    <div class="content">Segera isi Tracer Study</div>
                </div>

            </li>

            <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?php echo base_url();?>uploads/web/<?=$slider2 ?>" alt="slider-image">

                <div class="tp-caption sft desc-slide center color-white color-full" data-x="770" data-y="279" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">
                    <div class="title main-color-1 font-2"><?php echo get_config_app(2)?></div>
                    <div class="content">Ayo bergabung bersama kami </div>
                </div>
            </li>
        </ul>
    </div>
</div> <!-- /.tp-banner-container -->

<div class="header-overlay-content header-overlay-scroller">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <section class="un-post-scroller un-post-scroller-2901 " data-delay="0">
                    <div class="section-inner-no-padding">
                        <div class="post-scroller-wrap">
                            <div class="post-scroller-carousel" data-next=".post-scroller-down" data-prev=".post-scroller-up">
                                <div class="post-scroller-carousel-inner">
                                    <div class="item post-scroller-item active">
                                        <div class="scroller-item-inner">
                                          
                                            <div class="scroller-item-content post-item-mini">
                                                <div class="row">
                                                    
                                                    <div class="col-md-3">
                                                        <div class="item-thumbnail">
                                                            <a href="#">
                                                                <img src="<?php echo base_url('assets/web/images/clipboard.png');?>" alt="image">
                                                                <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                                <div class="thumbnail-hoverlay-cross"></div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-9">
                                                        <h4><b>Jika Anda Alumni :</b></h4>
                                                        <div class="post-excerpt-mini">
                                                            <p>1. Gunakan NISN Anda untuk melakukan pendaftaran di website. Cek di laman <a href="http://nisn.go.id" target="_blank">NISN</a></p>
                                                            <p>2. Jika ada permasalahan dalam melakukan pendaftaran, silahkan kirim pertanyaan di <b>Buku Tamu</b></p>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div><!--/post-item-mini-->
                                        </div>
                                    </div><!--/post-scroller-item--> 
                                </div>
                            </div>
                            
                        </div>
                    </div><!--/section-inner-->
                </section><!--/u-post-carousel-->
            </div>
        </div>
    </div>
</div>